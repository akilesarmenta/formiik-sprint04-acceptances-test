var should = require("should")
var request = require("request")
var expect = require("chai").expect
var util = require("util")
var zlib = require('zlib');

var urlSecurity = "http://rc.formiik.com:3030/SecurityPipeRest.svc"

var parameters = {
    "ClientName": "emlink",
    "UserName": "mau"
}

console.log('----- ESCENARIOS ' + 'zip'.toUpperCase() + ' -----')

describe('Zip Cadena', function (){
	it('Cadena Zipeada',function (done){
		console.log('Value parameters: ' + JSON.stringify(parameters))
		var zipParameters = zlib.gzipSync(JSON.stringify(parameters)).toString('base64')
		var request = {
			"Parameters":zipParameters,
			"Language":"es"
		}
		console.log("Request value: " + JSON.stringify(request))
		done()
	})	
})